package API;

import API.steps.WildBerriesSteps;
import io.qameta.allure.Epic;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.IsEqual.equalTo;

@Epic("API")
public class WildberriesTest {
    WildBerriesSteps wbSteps;

    @BeforeClass
    public void setUp() {
        wbSteps = new WildBerriesSteps();
    }

    @DataProvider
    public Object[][] buildMap() {
        Map<String,String> params = new HashMap<>();
        params.put("curr", "rub");
        params.put("dest", "-1029256,-102269,-2162196,-1257786");
        params.put("lang", "ru");
        params.put("query", "iphone");
        params.put("locale", "ru");
        params.put("resultset", "catalog");

        return new Object[][]{
                {params}
        };
    }

    @Story("Wildberries search")
    @Test(description = "Проверка запроса поиска товаров", dataProvider = "buildMap")
    public void checkSearch(Map<String, String> params){
        List<String> listOfSearchResult = wbSteps
                .getSearchResult(params, "https://search.wb.ru/exactmatch/ru/common/v4")
                .getList("data.products.name");
        wbSteps.checkResult(listOfSearchResult, "iPhone");
    }

    @Story("Wildberries basket")
    @Test(description = "Добавление товара в корзину", dataProvider = "buildMap")
    public void checkAdditionToBasket(Map<String, String> params) {
       String productId = wbSteps.getProductId(params, "https://search.wb.ru/exactmatch/ru/common/v4");

        wbSteps.additionToBasket(productId)
                .then()
                .statusCode(200)
                .body("value.data.basket.basketItems[0].cod1S", equalTo(Integer.parseInt(productId)));

    }
}
