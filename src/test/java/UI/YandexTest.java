package UI;

import io.qameta.allure.Story;
import org.testng.annotations.Test;

public class YandexTest extends YandexBaseTest {


    @Story("Проверка поискового движка")
    @Test(description = "Проверка поиска ноутбука Lenovo  стоимостью меньше чем 30000" )
    public void searchEngineForNotebooksTest() {
        market
                .clickOnSection("Ноутбуки")
                .verification().checkSectionOpened("Ноутбуки");

        market
                .setMaximumPrice(30000)
                .chooseManufacturer("Lenovo")
                .verification().checkResult("Lenovo");

       String storedItem = market.getShownOnFirstPageItems().get(0);

        market.openFirstFoundItem()
                .verification().checkFoundNameCorrespondsToTheStoredValue(storedItem);

    }

    @Story("Проверка поискового движка")
    @Test(description = "Проверка поиска планшета Xiaomi стоимостью между 20000-95000")
    public void searchEngineForTabletsTest() {
        market
                .clickOnSection("Планшеты")
                .verification().checkSectionOpened("Планшеты");

        market
                .setMinimalPrice(20000)
                .setMaximumPrice(95000)
                .chooseManufacturer("Xiaomi")
                .verification().checkResult("Xiaomi");

        String storedItem = market.getShownOnFirstPageItems().get(0);

        market.openFirstFoundItem()
                .verification().checkFoundNameCorrespondsToTheStoredValue(storedItem);
    }
}
