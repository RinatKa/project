package UI;

import UI.managers.WebDriverManager;
import UI.pages.YandexMarketPage;
import UI.pages.YandexPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class YandexBaseTest {
    protected WebDriverManager manager = new WebDriverManager();
    protected YandexMarketPage market = new YandexMarketPage();
    protected YandexPage yandex = new YandexPage();

    @BeforeMethod
    public void setup() {
        manager.open("https://ya.ru");

        yandex
                .verification().checkYandexMainPageOpened();

        yandex
                .clickOnYandexMarketLogo();

        market
                .verification().checkYandexMarketMainPageOpened();

        market
                .clickOnCatalogueButton()
                .clickOnComputersSection()
                .verification().checkSectionOpened("Ноутбуки и компьютеры");

    }

    @AfterMethod
    public void tearDown() {
        manager.close();
    }
}
