package UI.managers;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;

public class WebDriverManager {
        static {
            Configuration.browserSize = "1920x1080";
            Configuration.browser = "chrome";
            Configuration.browserVersion = "108";
            Configuration.browserPosition = "0x0";
            Configuration.timeout = 30 * 1000;
            Configuration.pageLoadTimeout = 30 * 1000;
            Configuration.savePageSource = false;
            Configuration.holdBrowserOpen = false;
        }

        @Step("Открываем браузер на странице {url}")
        public void open(String url) {
            Selenide.open(url);
        }

        @Step("Закрываем браузер")
        public void close() {
            Selenide.closeWebDriver();
        }
}
