package UI.pages;

import com.codeborne.selenide.*;
import io.qameta.allure.Step;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.time.Duration;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class YandexMarketPage {

    private final SelenideElement openedItemTitle = $x("//h3");
    private final SelenideElement catalogueButton = $x("//button[@id = 'catalogPopupButton']");
    private final SelenideElement computersSection = $x("//span[text() = 'Ноутбуки и компьютеры']");
    private final SelenideElement yandexMarketLogo = $x("//a[@id = 'logoPartMarket']");
    private final SelenideElement maximumPriceInputField = $x("//label[text() = 'Цена, ₽ до']/following-sibling::div//input");
    private final SelenideElement minimalPriceInputField = $x("//label[text() = 'Цена, ₽ от']/following-sibling::div//input");
    private final SelenideElement headerSearchInputField = $x("//div[@data-apiary-widget-id = '/header/search']//input[@id = 'header-search']");
    private final SelenideElement loader = $x("//span[@aria-label='Загрузка...']");
    private final ElementsCollection allItemsOnPage = $$x("//h3[@data-zone-name = 'title']");
    private final ElementsCollection filteredNoteBookList = $$x("//a[contains(@href, 'product--noutbuk')]");

    @Step("Get shown on first page items")
    public List<String> getShownOnFirstPageItems() {
        loader.should(Condition.disappear);
        return allItemsOnPage.shouldHave(CollectionCondition.sizeGreaterThan(0)).texts();
    }

    @Step("ОТкрыть первую найденную запись")
    public YandexMarketPage openFirstFoundItem() {
        allItemsOnPage.get(0).click();
        return this;
    }


    @Step("Enter term to search input field")
    public YandexMarketPage enterTermToSearchField(String term) {
        headerSearchInputField.clear();
        headerSearchInputField.val(term).pressEnter();
        return this;
    }


    @Step("Выбрать производителя")
    public YandexMarketPage chooseManufacturer(String manufacturer) {
        $x(String.format("//fieldset//span[text() = '%s']", manufacturer)).click();
        return this;
    }

    @Step("Установить максимальну стоимость {maximumPrice}")
    public YandexMarketPage setMaximumPrice(int maximumPrice) {
        maximumPriceInputField.clear();
        maximumPriceInputField.val(String.valueOf(maximumPrice));
        loader.should(Condition.disappear);
        return this;
    }

    @Step("Установить минимальную стоимость {minimalPrice}")
    public YandexMarketPage setMinimalPrice(int minimalPrice) {
        minimalPriceInputField.clear();
        minimalPriceInputField.sendKeys(String.valueOf(minimalPrice));
        return this;
    }

    @Step("Click on catalogue button")
    public YandexMarketPage clickOnCatalogueButton() {
        catalogueButton.click();
        return this;
    }

    @Step("Выбор секции компьютеры")
    public YandexMarketPage clickOnComputersSection() {
        computersSection.click();
        return this;
    }

    @Step("Выбрать секцию {section}")
    public YandexMarketPage clickOnSection(String section) {
        $x(String.format("//a[text() = '%s']", section)).click();
        return this;
    }

    public Verification verification() {
        return new Verification();
    }

    public class Verification {
        private Verification() {
        }

        @Step("Проверка найденой записи")
        public Verification checkFoundNameCorrespondsToTheStoredValue(String stored) {
            Assert.assertTrue(stored.contains(openedItemTitle.text()), "Ожидание: " + stored + " не равно актуальному: " + openedItemTitle.text());
            return this;
        }

        @Step("Проверяем что страница ЯндексМаркет открыта")
        public Verification checkYandexMarketMainPageOpened() {
            Assert.assertTrue(yandexMarketLogo.shouldBe(Condition.visible).isDisplayed()
                    , "ЯндексМаркет не открыт");
            Assert.assertTrue(WebDriverRunner.url().contains("market.yandex.ru")
                    , "ЯндексМаркет не открыт");
            return this;
        }


        @Step("Проверяем что записи соответствуют фильтрации")
        public Verification checkResult(String item) {
            SoftAssert soft = new SoftAssert();
            loader.should(Condition.disappear);
            filteredNoteBookList.asDynamicIterable().stream().filter(x-> x.getText().contains(item)).forEach(x->
                     soft.assertTrue(x.getText().contains(item), x.getText() + " не содержит " + item));
            soft.assertAll();

            return this;
        }

        @Step("Проверяем что секиця {section} открыта")
        public Verification checkSectionOpened(String section) {
            Assert.assertTrue($x(String.format("//h1[contains(text(), '%s')]", section))
                            .shouldBe(Condition.visible, Duration.ofSeconds(10)).isDisplayed()
                    , section + "секция не открыта");
            return this;
        }

    }

}

