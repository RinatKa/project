package UI.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.testng.Assert;

import static com.codeborne.selenide.Selenide.*;

public class YandexPage {

    private final SelenideElement yandexMarketLogo = $x("//a[contains(@data-statlog,  'market')]");
    private final SelenideElement yandexAllServices = $x("//a[@title= 'Все сервисы']");
    private final SelenideElement yandexLogo = $x("//a[@aria-label = 'Яндекс']");

    @Step("Перейти в ЯндексМаркет")
    public YandexPage clickOnYandexMarketLogo() {
        yandexAllServices.click();
        yandexMarketLogo.should(Condition.visible).click();
        Selenide.switchTo().window(1);
        return this;
    }

    public YandexPage.Verification verification() {
        return new YandexPage.Verification();
    }

    public class Verification {
        private Verification() {
        }

        @Step("Проверка открытия главной страницы Яндекс")
        public YandexPage.Verification checkYandexMainPageOpened() {
            Assert.assertTrue(yandexLogo.isDisplayed(), "Яндекс не открыт");
            return this;
        }

    }

}
