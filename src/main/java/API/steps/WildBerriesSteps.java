package API.steps;

import API.specification.RequestSpecification;
import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;


public class WildBerriesSteps {

    @Step("Получаем Результат запроса")
    public JsonPath getSearchResult(Map<String, String> params, String baseURI) {

        return given().spec(RequestSpecification.reqSpecJSON(baseURI))
                .queryParams(params)
                .when()
                .get("/search")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .jsonPath();
    }


    @Step("Проверяем Результат")
    public void checkResult(List<String> result, String expect){
        SoftAssert soft = new SoftAssert();

        result.forEach(x-> soft.assertTrue(x.contains(expect), x + " не содержит " + expect));
        soft.assertAll();
    }

    @Step("Получаем ID Продукта")
    public String getProductId(Map<String, String> params, String baseURI) {
       return getSearchResult(params, baseURI).getString("data.products[0].id");
    }

    @Step("Добавление продукта в корзину")
    public Response additionToBasket(String productId) {
        Map<String, String> form = new HashMap<>();
        form.put("basketItems[0][cod1S]", productId);
        form.put("currency", "RUB");

        return given()
                .spec(RequestSpecification.reqSpecFormData("https://ru-basket-api.wildberries.ru/webapi/lk/basket/data"))
                .formParams(form)
                .when()
                .post();
    }

}
