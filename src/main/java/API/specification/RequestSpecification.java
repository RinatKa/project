package API.specification;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;


public class RequestSpecification {
    public static io.restassured.specification.RequestSpecification reqSpecJSON(String baseUri) {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder  .setRelaxedHTTPSValidation()
                            .setBaseUri(baseUri)
                            .setContentType(ContentType.JSON)
                            .setAccept(ContentType.JSON)
                            .setUrlEncodingEnabled(true)
                            .addFilter(new AllureRestAssured());

        return requestSpecBuilder.build();
    }

    public static io.restassured.specification.RequestSpecification reqSpecFormData(String baseUri) {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder  .setRelaxedHTTPSValidation()
                .setBaseUri(baseUri)
                .setUrlEncodingEnabled(true)
                .addFilter(new AllureRestAssured());

        return requestSpecBuilder.build();
    }
}
