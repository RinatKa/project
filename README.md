Проект содержит в себе UI  и API автотесты.
 1. Необходимо скачать JDK 17  и установить  Версии по ссылке: https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html
 2. Необходимо скачать и установить среду разработки. 
    Ссылка на Intellij https://www.jetbrains.com/ru-ru/idea/download/download-thanks.html?platform=windows&code=IIC
 3. Необходимо склонировать данный репозиторий на локальную машину и открыть через Intellij   
 4. Собрать проект командой gradle build установить в качастве SDK 17 java version
 5. Для запуска API тестов необходимо перейти по пусть в каталоге src/java/API/WildberriesTest.java
 6. И запустить автотесты по нажатию Кнопки запуска напротив названия класса
 7. Для запуска UI тестов необходимо перейти по пусть в каталоге src/java/UI/YandexTest.java
 8. И запустить автотесты по нажатию Кнопки запуска напротив названия класса
